
We will continue using the chick weight dataset from the previous problem. As a reminder, run these lines of code in R to prepare the data:

```{r}
data(ChickWeight)
chick <- reshape(ChickWeight,idvar=c("Chick","Diet"),timevar="Time",direction="wide")
chick <- na.omit(chick)
```

Make a strip chart with horizontal jitter of the chick weights from day 4 over the different diets:

```{r}
stripchart(chick$weight.4 ~ chick$Diet, method="jitter", vertical=TRUE)
```

Suppose we want to know if diet 4 has a significant impact on chick weight over diet 1 by day 4. It certainly appears so, but we can use statistical tests to quantify the probability of seeing such a difference if the different diets had equal effect on chick weight.

## QUESTION 2.1  (1 point possible)

Save the weights of the chicks on day 4 from diet 1 as a vector 'x'. Save the weights of the chicks on day 4 from diet 4 as a vector 'y'. Now perform a t test comparing x and y (in R the function t.test(x,y) will perform the test). Now, perform a Wilcoxon test of x and y (in R the function wilcox.test(x,y) will perform the test). Note that a warning will appear that an exact p-value cannot be calculated with ties (so an approximation is used, which is fine for our purposes).

Now, perform a t-test of x and y, after adding a single chick of weight 200 grams to 'x' (the diet 1 chicks). What is the p-value from this test? The p-value of a test is available with the following code:

```{r}
TODO
t.test(x,y)$p.value
```

Do the same for the Wilcoxon test. Note that the Wilcoxon test is robust to the outlier (and in addition, has less assumptions that the t-test on the distribution of the underlying data).

## QUESTION 2.2  (1 point possible)

We will now investigate a possible downside to the Wilcoxon-Mann-Whitney test statistic. Using the following code to make three boxplots, showing the true Diet 1 vs 4 weights, and then two altered versions: one with an additional difference of 10 grams and one with an additional difference of 100 grams. (Use the x and y as defined above, NOT the ones with the added outlier.)

```{r echo=FALSE}
par(mfrow=c(1,3))

boxplot(x,y)

boxplot(x,y+10)

boxplot(x,y+100)
```

What is the difference in t-test statistic (the statistic is obtained by t.test(x,y)$statistic) between adding 10 and adding 100 to all the values in the group 'y'? So take the the t-test statistic with x and y+10 and substract away the t-test statistic with x and y+100. (The value should be positive).

```{r}
TODO
```

Now examine the Wilcoxon test statistic for x and y+10 and for x and y+100. Because the Wilcoxon works on ranks, after the groups have complete separation (all points from group 'y' are above all points from group 'x'), the statistic will not change, regardless of how large the difference grows. Likewise, the p-value has a minimum value, regardless of how far apart the groups are. This means that the Wilcoxon test can be considered less powerful than the t-test in certain contexts, and with small significance levels (alpha). In fact for small sample sizes, the p-value can't be very small, even when the difference is very large. Compare:

```{r}
wilcox.test(c(1,2,3),c(4,5,6))

wilcox.test(c(1,2,3),c(400,500,600))
```

This issue becomes important in Course 3 on Advanced Statistics, when we discuss correction for performing thousands of tests, as is done in high-throughput biological assays.

