---
title: "Probability Distributions #1"
author: "AppanP"
date: "14 February 2015"
output: html_document
---

[TOC]

Let's start by loading in the data that Rafa used in the video. Remember to click on "Raw" to download individual files from github.

```{r}
dat = read.csv("femaleMiceWeights.csv")
```

The observed difference between high fat diet and control was calculated like so:

```{r}
mean(dat[13:24,2]) - mean(dat[1:12,2])
```

Let's make a plot of these two groups: a strip chart of the weights. We're going to use some of the functions that we used from the assessments in Week 1, split() and soon sapply().

```{r}
s = split(dat[,2], dat[,1])
stripchart(s, vertical=TRUE, col=1:2)
```

Let's add the means to the plot as well:

```{r}
abline(h=sapply(s, mean), col=1:2)
```

## QUESTION 1.1  (1 point possible)

How many of the high fat mice weigh less than the mean of the control mice (chow)?

```{r}
TODO
```

## QUESTION 1.2  (1 point possible)

How many of the control mice weigh more than the mean of the high fat mice?

```{r}
TODO
```

## SAMPLE()

In the next video, Rafa will use the sample() function in R to generate random samples of a population.

Before watching that, let's try it out in your R session. Continuing from the code above where we split the weights by diet, s[["hf"]] or equivalently s$hf gives the weights for the high fat diet mice. Save that to a new vector 'highfat':

```{r}
highfat = s[["hf"]]
```

Now print the values for highfat:

```{r}
highfat
```

Now try the following command:

```{r}
sample(highfat, 6)
```

You can press the UP arrow and ENTER to reproduce the command multiple times. The function sample() goes into the 'highfat' vector and chooses 6 values at random (6 is the second argument to sample, which is called the 'size' of the sample). With the default argument settings, sample() will only pick an observation once, so it's like drawing cards from a deck without putting the cards back in the deck. Because all of the value in 'highfat' are unique, they will only show up one time in the sample of 3 using the above line of code.

In the R console, type out ?sample and hit ENTER. Read the argument descriptions for x, size and replace.

There is an argument to sample() called 'replace' which toggles whether or not an observation can be chosen more than once (whether to replace the observations back in the population after they are chosen once). The default setting for 'replace' is set to FALSE, so if we don't say anything, the sample() function will not choose an observation more than once.

Try the following command a few times:

```{r}
sample(highfat, 6, replace=TRUE)
```

You should notice that some of the time, there are repeated values in the sample. This is because we changed the setting of 'replace' to allow for multiple draws of the same obseration.

In the following video, we will be using sample() with replace=FALSE.

## QUESTION 1.3  (1 point possible)

Finally, we have a short problem showing a trick with calculating proportions. If we have a logical vector, for example produced by the logical expression,

```{r}
highfat	> 30
```

... a convenient trick is to use this logical vector directly in functions which usually work on numeric values. What happens inside of the R function is that the TRUE is turned into a 1, and the FALSE is turned into a 0. This happens automatically, so you don't need to convert the vector type yourself:

```{r}
as.numeric(highfat > 30)
```

For example if we want to know the number of the high fat diet mice that weigh over 30:

```{r}
sum(highfat > 30)
```

The proportion of high fat diet mice over 30 is the sum of high fat diet mice over 30 divided by the number of high fat diet mice, in other words, the mean of a vector of 1s and 0s. What is the proportion of high fat diet mice over 30?

```{r}
TODO
```

