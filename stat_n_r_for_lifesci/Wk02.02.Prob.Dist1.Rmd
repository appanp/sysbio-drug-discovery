
Now let's load the population data for the control mice (link is here).

```{r}
population = read.csv("femaleControlsPopulation.csv")
```

Because this is just one column of data, as you can see with head(), let's just extract the one column as a vector:

```{r}
population = population[,1]
```

## QUESTION 2.1  (1 point possible)

What's the control population mean?

```{r}
TODO
```

## RANDOM SAMPLES

The idea of a random variable, is that we pick at random some samples from the population and then calculate the mean. So depending on which samples we pick, the mean changes, and therefore we say that the mean of a sample of the population is a random variable. In R, we can make a random sample using the sample() function:

```{r}
sample(population, 12)
```

We can calculate the mean for a sample in the normal way:

```{r}
mean(sample(population, 12))
```

In the video Rafa showed how to perform the whole experiment (pick two groups of 12 and calculate the difference in means) 10,000 times using a for loop. Here we'll try a more concise way to do many random samples, using the replicate() function in R. The replicate() function takes two arguments, which are the number of times to replicate, and then an expression: the command you want to replicate. First let's try out making a random sample of one group of 12 mice from the population and calculating the mean:

```{r}
sampleMean = replicate(10000, mean(sample(population, 12)))
head(sampleMean)
```

We can't quiz you on the values you get, because these are random and will be different on everyone's computer! (Although there is an extra command in R to make sure we all get the same result from a random sample command.) Let's plot the different means that we got, spreading them out one at a time along the x axis:

```{r echo=FALSE}
plot(sampleMean)
```

We can also use replicate() to perform the same operation that Rafa did in the video: calculating the difference between two random samples of 12 from the control mice:

```{r}
null = replicate(10000, mean(sample(population, 12)) - mean(sample(population, 12)))
```

Take a look at a few of these 10,000 differences, and plot them along the x-axis, like we did previously for the mean of 12 random mice:

```{r}
head(null)
plot(null)
```

