
This repository contains source code for the following:

1. R code snippets for specific tasks related to Genomics
1. Python code snippets for the same set of tasks
1. Exercises from the PH525 EdX series of courses
